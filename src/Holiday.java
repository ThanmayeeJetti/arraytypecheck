import java.util.ArrayList;
import java.util.Scanner;
public class Holiday {
    private String name;
    private int day;
    private String month;


    public Holiday(String name,int day,String month){
        this.name=name;
        this.day=day;
        this.month=month;
    }
    public boolean inSameMonth(Holiday h1,Holiday h2){
        if(h1.month.equals(h2.month))
            return true;
        else
            return false;
    }
    public   void avgDate(Holiday h[]){
        double res=0.0;
        double fin;
        int n=h.length;
        for(int i=0;i<n;i++){
            res=res+h[i].day;
        }
        fin=res/n;
        System.out.println(fin);

    }
    public static void main(String args[]){
            Scanner s =new Scanner(System.in);
            int n=s.nextInt();
            Holiday h[]=new Holiday[n];
            for(int j=0;j<n;j++) {
                String name=s.nextLine();
                int day= s.nextInt();
                String month=s.nextLine();
                Holiday hol= new Holiday(name,day,month);
                h[j]=hol;

            }
            Holiday h3=new Holiday("h1",23,"July");
            Holiday h4=new Holiday("h2",23,"July");
            System.out.println(h3.inSameMonth(h3, h4));
            h3.avgDate(h);
    }
}
