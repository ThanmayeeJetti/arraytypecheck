interface A{
    void play();
}
interface B{
    void play();
}

public class multiple {
    public static class C implements A,B{
        public void play(){
            System.out.println("Playing");
        }
    }
    public static void main(String args[]){
        C ob=new C();
        ob.play();
    }
}
