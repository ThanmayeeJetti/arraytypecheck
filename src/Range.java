import java.util.Scanner;

public class Range {
    public static void main(String args[]){
        Scanner s=new Scanner(System.in);
        int n=s.nextInt();
        int arr[]=new int[n];
        for(int i=0;i<n;i++){
            arr[i]=s.nextInt();
        }
        int maxi=-99999;
        int mini=999999;
        for(int i=0;i<n;i++){
            if(arr[i]>maxi)
                maxi=arr[i];
            if(arr[i]<mini)
                mini=arr[i];
        }
        int range=maxi-mini;
        System.out.println(range);

    }
}
